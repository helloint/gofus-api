# Gofus-API

Gofus-API is the Gofus repository containing the [OpenApi V2 Specification](https://swagger.io/docs/specification/2-0/basic-structure/) file (formerly swagger) for Gofus.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
### Community tools

Join us on [Slack](https://join.slack.com/t/gofus/shared_invite/enQtNzc1ODk2NDYzMjgzLTU0NzM2Mzk1YTBlZjhiNGE3YTQyMjc1NmYyYzY4ODVmMzVjZTI0MTk5Mzk5ZGIxNDQwNGE3ZDM2ZWFiM2I0NmY) or [Discord](https://discord.gg/xgNfzVJ) !

Check our [Trello](https://trello.com/b/VZBgmYfO/gofus) ! [Team Invitation Link](https://trello.com/invite/b/VZBgmYfO/9de3394da289a6e6d72063e44314e4dd/gofus)

Check the [Gofus Proposal](https://docs.google.com/document/d/1KUc18DVd6pT7niy608H0nvGO8Rt7bsbxklc0YHvisH0/edit?usp=sharing) specification document (wip) to learn more about the project.

## License
[MIT](https://choosealicense.com/licenses/mit/)
